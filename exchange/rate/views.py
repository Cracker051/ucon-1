from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
import requests
import json
KEY = 'P8Gplxndp2gnkVX32ypkR4hcAH5p3UJs'
ALLOWED_CURRENCY = ('USD', 'EUR', 'GBP', 'UAH')


@api_view(['POST',])
def exchange(request, *args):
    """
    Convert the currency

    Parametrs (from request body)
    -----------
        convert_from : str
            ISO currency code you want to exchange
        amount : int
            Amount you want to exchange
        convert_to : str
            ISO currency code you want to get
    """
    try:
        # Get 'convert_from' from body, removing any spaces,apply upper case
        c_from = request.data.get('convert_from', None).strip().upper()
        # Get 'amount' from body, casting to int
        c_amount = int(request.data.get('amount', None))
        # Get 'convert_to' from body, removing any spaces,apply upper case
        c_to = request.data.get(
            'convert_to', None).strip().upper()   # Get 'to' from body
    except:
        return Response({'Check the inputed data!'}, status=status.HTTP_400_BAD_REQUEST)
    if c_from not in ALLOWED_CURRENCY or c_to not in ALLOWED_CURRENCY:  # If user type not allowed currency
        return Response({f'You can exchange only {ALLOWED_CURRENCY}. Check the inputed data!'}, status=status.HTTP_400_BAD_REQUEST)
    else:
        RATES = requests.get(
            url=f'https://api.apilayer.com/fixer/convert?to={c_to}&from={c_from}&amount={c_amount}', headers={'apikey': KEY}, data={})  # Passing variables through url
        if RATES.status_code != 200:  # If unhandled error happened
            return Response(RATES.text, status=RATES.status_code)
        info_about = json.loads(RATES.text)
        # Extracting info
        output = {
            'currency': info_about['query']['to'],
            'amount': info_about['result'],
            'rate': info_about['info']['rate']
        }
        return Response(output, status=RATES.status_code)
